# Payments iOS SDK

## Installation

### CocoaPods

[CocoaPods](https://cocoapods.org) is a dependency manager for Cocoa projects. For usage and installation instructions, visit their website.
To integrate the SDK into your Xcode project using CocoaPods, first add the souces to the top of your  `Podfile`:

```ruby
source 'https://bitbucket.org/geopagos-sdk/ios-specs.git'
source 'https://github.com/CocoaPods/Specs.git'
```

Then add the pod to your target:

```ruby
  pod 'Payments', '~> 10.2.1'
```

Example full  `Podfile`, if your target is named `MyTarget`:

```ruby
source 'https://bitbucket.org/geopagos-sdk/ios-specs.git'
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '10.0'

target 'MyTarget' do
  use_frameworks!

  # Pods for MyTarget
  pod 'Payments', '~> 10.2.1'

end
```

## Reference

For class and method referenes, checkout this repository and open `docs/index.html`

## Usage

- Import the SDK

```swift
import Payments
```

- Create a PaymentsConfiguration for configure the endpoint, application key and readerInstallers (optionally a logger object, etc)

```swift

    let paymentsConfiguration = PaymentsConfiguration.Builder(endpoint: "https://www.example.com", applicationKey: TOKEN, readerInstallers: [<QposReaderInstaller>, <MagicPosReaderInstaller>])
            .setLogger(logger: delegate)
            .build()
            
    PaymentsSDK.configure(paymentsConfiguration: paymentsConfiguration)
```

Note: To create readerInstallers is necessary import the module corresponding to each Reader

For Qpos

```swift
import QPosHardware
    
    let magicPosReaderInstaller = MagicPosReaderInstaller()
```

For Magic pos

```swift
import MagicPosHardware
    
    let qposReaderInstaller = QposReaderInstaller()
```

- Scan for devices

```swift
let deviceService = DeviceScanService()

deviceService.scan { (result: DeviceResult) in
    switch result {
    case .success(let list, let isScanning):
        for device: Device in list {
            //...
        }
    case .error(let type, let isScanning):
        //handle errors
    }
}
```

- Start a Transaction

```swift
TransactionIntentFactory.createTransactionIntent(type: .payment, delegate: delegate)
```

### Reader Update

Reader configuration can be updated using the appropriate configuration files provided by Geopagos team.

To create a `ReaderConfigurator`, use the `ReaderConfiguratorFactory`

```swift
let readerConfigurator = ReaderConfiguratorFactory.createConfigurator(delegate: delegate)
```
Where the delegate implements the `ReaderConfiguratorDelegate` protocol.
Each delegate callback will guide you into completing the configuration, and asking for needed data.

On the `onConfigurationRequired` method, you must provide a `ReaderConfigurationFiles` objects, exists several factories according the configuration type and the reader used, firmware configuration can be done as follows:

for Qpos 
```swift
QposReaderConfigurationFactory.qposFirmwareFiles(firmware: url)
```
where firmware contain the bundle file provided from geopagos

for magicpos 

```swift
MagicPosReaderConfigurationFactory.qposFirmwareFiles(firmware: url)
```
firmware same case that qpos

In the case that the bundle file does not contain a configuration that matching with the configuration type and the reader requested, it will be returned a `.invalidConfiguration` error.

### Qpos mini Firmware update (Edge case)
In the case Firmware update failed and reader to pass to a boot state (not response to press any key) it is necessary to use a special device for finish the update

```swift
let qposMiniFirmwareRestoreDevice = QposMiniFirmwareRestoreDevice(device: device, readerInfo: readerInfo)
```

This device is passed in `ReaderConfiguratorDelegate.onDeviceRequired(provideDevice:)`

#### Keeping the Reader charging
Some configuration updates result in a lengthy operation (specially firwmare updates) that can take between 4 and 10 minutes to complete. Because of this, it's recommended to ask the user to keep the reader connected to a power source before start updating to avoid a disconnection in the middle of the update due to low battery charge.

